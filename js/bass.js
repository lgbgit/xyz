const isMobile = navigator.userAgent.match(
  /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
);
if (!isMobile) {
  // 移动端
  console.log(window.location);
  window.location.href = `${window.location.origin}/404.html`;
}
function convertRootFontSizeToRem() {
  const rootSize = document.documentElement.clientWidth; // 获取根元素宽度
  const pxRemValue = 50; // 设计图1rem对应的px值，单位为px
  const remValue = 375 / pxRemValue; // 计算根元素尺寸对应的rem值
  const htmlFontSize = (rootSize / remValue).toFixed(2);
  console.log(rootSize, remValue, htmlFontSize);
  document.documentElement.style.fontSize = `${htmlFontSize}px`; // 设置根元素字体大小为计算得到的rem值
}
convertRootFontSizeToRem();
// 监听页面尺寸变化事件
window.addEventListener("resize", convertRootFontSizeToRem);
